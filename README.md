# LuVirt - Virtualization Software for Lua

LuVirt is an experimental hypervisor for Lua. It is currently being developed for PUC-Rio Lua 5.1.5, PUC-Rio Lua 5.2.4, PUC-Rio Lua 5.3.3 and LuaJIT 2.0.x.

As with any virtualization software, different versions are needed for different instruction sets, even if such versions may share a common core. Because Lua is a dynamic language, the "luvirt.bc" module has different submodules for different bytecode implementations, which are loaded dynamically on the fly.

Note: Bytecode is very binary-specific. Different Lua binaries may use different bytecode types. It is safe to strip the bytecode module entirely, and that simply disallows bytecode loading.

## Examples

Injecting functions into a VM:

    local luvirt = require 'luvirt'
    
    local vm = luvirt.newstate()
    local t = vm:newtable() -- wrapper table
    local f = vm:load("return function(x) return x+1 end")() -- wrapper function
    t["f"] = f -- this calls __newindex
    vm:setglobal("mylib", t) -- this unwraps t
    print(vm:load("return mylib.f(3)")()) --> 4

Using wrapped functions from a VM:

    local luvirt = require 'luvirt'
    
    local vm = luvirt.newstate()
    vm:setglobal("print", function(...) print(...) end) -- can also use vm:setglobal("print", print) directly
    vm:load("print('Hello, world!')")() --> Hello, world!

Wrapped functions get called in the hypervisor thread, which means they aren't affected by debug hooks, environments, etc. However, they also receive wrapped arguments, so only luvirt-aware code works with them. For example, to provide a print which does get affected by the VM environment:

    local luvirt = require 'luvirt'
    
    local vm = luvirt.newstate()
    local vm_print = vm:load([[local write, select = ...; return function(...)
      local t = {...}
      local n = select('#', ...)
      -- note that print uses the global tostring
      if n > 0 then
        write(tostring(t[i]))
      end
      for i=2, n do
        write('\t', tostring(t[i]))
      end
      write('\n')
    end]])(io.write, luvirt.select)
    vm:setglobal("tostring", luvirt.tostring)
    vm:setglobal("print", vm_print) -- could also set print in the loaded chunk
    vm:load("print('Hello, world!')")() --> Hello, world!
    vm:setglobal("tostring", vm:load("return function() return '???' end")())
    vm:load("print('Hello, world!')")() --> ???

## API

### `luvirt.newstate()`

This function creates and returns a new state/VM, with a clean (empty) environment.

### `VM:load(ld)`

This function loads a chunk in the given state/VM, and returns a handle to it. `ld` should be a string or a function.

### `VM:call(f, ...)`

This function calls a wrapped function. Note that, given a wrapper function `f`, `f(...)` is sugar for `VM:call(f, ...)`.

### `VM:newtable()`

This function creates a new table in the given state/VM, and returns a handle to it.

### `VM:getmetatable(obj)`

Equivalent to debug.getmetatable, but in the context of the state/VM. Returns a wrapper table.

### `VM:setmetatable(obj, mt)`

Equivalent to debug.setmetatable, but in the context of the state/VM. `mt` must be a wrapper table or nil.

### `VM:iswrapper(obj)`

Returns `true` if the given object is a wrapper object, `false` otherwise.
