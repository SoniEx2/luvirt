-- require 'luvirt'

if coroutine.running() then
  local co, main = coroutine.running()
  if not main then
    error("Cannot run luvirt hypervisor outside main thread.")
  end
end

-- The Singleton.
-- This is needed for table wrapping.
local SINGLETON = {}

-- locals
local metatable = {
  set = setmetatable,
  get = getmetatable,
  rawset = debug.setmetatable,
  rawget = debug.getmetatable,
}

local coroutine = {
  create = coroutine.create,
  resume = coroutine.resume,
  status = coroutine.status,
  yield = coroutine.yield,
  running = coroutine.running
}

local type = type
local error = error

-- custom luvirt stuff (initialization code)
local luvirt = {
  sandboxed = {}
}
do
  local sandboxed = luvirt.sandboxed
  
  do local dummy, bc = require 'luvirt.bc' if dummy then luvirt.bc = bc end end
  
  if luvirt.bc and luvirt.bc.isVmKnown() then
    -- TODO
    -- The idea is to pass the bytecode through a decompiler
    -- and then pass it through a compiler again.
  else
    -- Deny bytecode loading.
    -- NOTE: Environment validation is handled elsewhere.
    local loadstring, load = loadstring, load
    if loadstring == load or not loadstring then
      -- Lua 5.2+ or LuaJIT
      function sandboxed.load(ld, src, mode, env)
        if type(ld) == "string" then
          return load(ld, src, "t", env)
        elseif type(ld) == "function" then
          -- TODO special case this so it doesn't break some
          -- assumptions.
        end
      end
    else
      local setfenv = setfenv
      assert(setfenv)
      -- Lua 5.1
      function sandboxed.load(ld, src, mode, env)
        if type(ld) == "string" then
          if ld:sub(1,1) == "\27" then -- probably bytecode
            return nil, "attempt to load a binary chunk"
          end
          local chunk, errmsg = loadstring(ld, src)
          if not chunk then return chunk, errmsg end
          if env then setfenv(chunk, env) end
          return chunk
        elseif type(ld) == "function" then
          -- TODO special case this so it doesn't break some
          -- assumptions.
        end
      end
    end
  end
end

-- module table
local HYPERVISOR = {}

-- VM methods
local VM = {}

-- the next 3 tables are separate to keep things clean

-- coroutine (coro -> metadata) tracking table
-- this NEEDS to be indexed by coroutines.
local cotrack = metatable.set({}, {__mode="k"})

-- function wrapper (wrapper -> func) tracking table
local ftrack = metatable.set({}, {__mode="k"})

-- table wrapper (wrapper -> table) tracking table
local ttrack = metatable.set({}, {__mode="k"})

-- metatables
local metatables = {
  VM = {__index = VM}, -- VM metatable
  TPROXY = { -- table wrapper metatable
    __index = function(self, k)
      -- TODO
    end,
    __newindex = function(self, k, v)
      -- TODO
    end,
  },
}

local function EXECUTOR(...)
  -- TODO
  while true do
    -- TODO
  end
end


function HYPERVISOR.newstate()
  local co = coroutine.create(EXECUTOR)
  local t = metatable.set({[SINGLETON]=co}, metatables.VM)
  cotrack[co] = {}
  return t
end

function VM:load()
  local function wrapper(...)
    -- idk if this is Lua 5.2-safe, but it seems to work
    return self:call(wrapper, ...)
  end
  local f = error("unimplemented") -- TODO
  ftrack[wrapper] = f
  -- TODO
  return wrapper
end

function VM:call(wrapped, ...)
  if not ftrack[wrapped] then error("Invalid argument #1 to VM:call()") end
  local f = ftrack[wrapped] -- unwrap it
  local co = self[SINGLETON]
  -- TODO
end

function VM:newtable()
  local proxy = {[SINGLETON]=self[SINGLETON]}
  ttrack[proxy] = {}
  metatable.set(proxy, metatables.TPROXY)
  -- TODO
  return proxy
end

return HYPERVISOR